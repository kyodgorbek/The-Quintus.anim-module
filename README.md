# The-Quintus.anim-module
Animation Module
Quintus.Anim =  function(Q) ()  {
    Q._animations = {};
     Q.animations = function(sprite,animations) {
	  if(!Q._animations[sprite],animations);
	_.extend(Q._animations[sprite],animations);
	};
	
	Q.animation = function(sprite,name) {
	 return Q._animations[sprite]  && Q._animations[sprite][name];
	
	Q.register('animation' , {
	   added: function()  {
	       var p = this.entity.p;
	       p.animation = null;
	       p.animationPriority = -1;
	       p.animationFrame = 0;
	       p.animationTime = 0;
	       this.entity.bind("step", this, "step");
	  },
           extend: { 
		play: function(name,priority) {
		   this.animation.play(name.priority) {
		}
	    },
	   step: function(dt) {}
             var entity = this.entity,
                   p = entity.p;
             if(p.animation) {
               var  anim Q.animation(p.sprite,p.animation),
                      rate = anim.rate  || p.rate,
	              stepped = 0;
	       p.animationTime  +=dt;
		if(p.animationChanged) {
		   p.animationChanged = false;
		} else {
		  p.animationChangedTime += dt;
                  if(p.animationTime > rate) {
                     stepped = Math.floor(p.animationTime / rate);
                     p.animationTime -= stepped * rate;			  
		     p.animationFrame +=stepped;
                  }
               }
              if(stepped > 0)  { 
		 if(p.animationFrame >= anim.frames.length) {
		    p.animationFrame = anim.frames.length - 1;
		    entity.trigger('animEnd');
		    entity.trigger('animEnd.' + p.animation);
		    p.animation = null;
		    p.animationPriority = -1;
		    if(anim.trigger) { 
		      entity.trigger(anim.trigger,anim.triggerData)
		   }
                   if(anim.next)  {this.play(anim.text,anim.nextPriority); }
                   return;
	        } else {
		   entity.trigger('animLoop');
		   entity.trigger('animLoop. ' + p.animation);
		   p.animationFrame = p.animationFrame % anim.frames.length;
                }  
	     },
	     
	play: function(name,priority) { 
	   var entity = this.entity,
	         p = entity.p;
	   priority = priority || 0;
	    if(name != p.animation && priority >= p.animationPriority)  {
	     p.animation = name;
	     p.animationChanged = true;
	     p.animationTime = 0;
	     p.animationFrame = 0;
	     p.animationPriority = priority;
	     entity.trigger('anim');
	     entity.trigger('anim' + p.animation);
	 }
      }
 });
};
